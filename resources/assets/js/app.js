require('./bootstrap');

Vue.component('category', require('./components/Category.vue'));
Vue.component('formulario', require('./components/Form.vue'));
Vue.component('carrusel', require('./components/Carousel.vue'));

axios.post("https://iz590xje3m.execute-api.us-west-2.amazonaws.com/prod/visitors",{
   page:  window.location.origin+window.location.pathname
});

const form = new Vue({
    el: '#formulario',
    data: {
    
    },
    
});

const cars = new Vue({
    el: '#imagenes',
    data: {
        categorias: window.vehiculos,
    },
    mounted(){
        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been re-rendered
            window.lazySizes.init(); 
        });
    }
    
});

const carrusel = new Vue({
    el: '#carrusel',
    data: {
        items: []
    },
    mounted() {
        if(typeof window.page_config.carrusel !== 'undefined')
            this.items = window.page_config.carrusel
        
        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been re-rendered
            window.lazySizes.init(); 
        });
    },
});

const aviso = new Vue({
    el: '#aviso',
    data: {
        aviso: true
    },
    mounted() {
        if(typeof window.page_config.aviso !== 'undefined')
            this.aviso = window.page_config.aviso
    },
});
