
window._ = require('lodash');
window.Popper = require('popper.js').default;

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
    console.log("failed to load jquery and bootstrap library")
}

try {
    window.lazySizes = require('lazysizes');
    require('lazysizes/plugins/parent-fit/ls.parent-fit');

    window.lazySizes.cfg.init = false;
} catch (error) {
    console.log("failed to load lazysizes library")
}

window.Vue = require('vue');


