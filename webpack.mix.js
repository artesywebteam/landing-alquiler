let mix = require('laravel-mix');

mix.setPublicPath('./public');

// extract vendor data
mix.extract([
    'jquery',
    'lodash',
    'popper.js',
    'bootstrap',
    'vue',
    'vuelidate',
    'axios',
    'lazysizes',
    'lazysizes/plugins/parent-fit/ls.parent-fit',
    'accounting',
    'moment',
    'moment-range',
]);

//JavaScript
mix.js('resources/assets/js/app.js','public/js/app.js');

//css
mix.sass('resources/assets/sass/vendor.scss','public/css/vendor.css');
mix.sass('resources/assets/sass/app.scss','public/css/app.css');

//copy
// mix.copy('resources/assets/sass/fonts/','public/fonts/');

if(mix.inProduction()){
    mix.version();
}
else{
    mix.browserSync('localhost:8000');
}


